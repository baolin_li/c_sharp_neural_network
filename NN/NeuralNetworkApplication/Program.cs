﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;

namespace NeuralNetworkApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("abcd");

            //Model nn = new Model("abcdefg");
            //Console.WriteLine(nn.aa);

            //nn.aa = "ggg";
            //Console.WriteLine(nn.aa);

            const int training_number = 10000;
            const int testing_number = 10000;

            string directoryPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;

            DirectoryInfo directorySelected = new DirectoryInfo(directoryPath);

            var files_to_read = directorySelected.GetFiles("*.gz");

            string training_image = Path.Combine(directoryPath, "train_image");
            string training_label = Path.Combine(directoryPath, "train_label");
            string testing_image = Path.Combine(directoryPath, "test_image");
            string testing_label = Path.Combine(directoryPath, "test_label");

            BinaryReader bytestream1 = new BinaryReader(new FileStream(training_image, FileMode.Open));
            var train_image_header = bytestream1.ReadBytes(16);
            var train_image = bytestream1.ReadBytes(784 * training_number);

            BinaryReader bytestream2 = new BinaryReader(new FileStream(training_label, FileMode.Open));
            var train_label_header = bytestream2.ReadBytes(8);
            var train_label = bytestream2.ReadBytes(1 * training_number);

            BinaryReader bytestream3 = new BinaryReader(new FileStream(testing_image, FileMode.Open));
            var test_image_header = bytestream3.ReadBytes(16);
            var test_image = bytestream3.ReadBytes(784 * testing_number);

            BinaryReader bytestream4 = new BinaryReader(new FileStream(testing_label, FileMode.Open));            
            var test_label_header = bytestream4.ReadBytes(8);
            var test_label = bytestream4.ReadBytes(1 * testing_number);

            Model neural_network = new Model(train_image, train_label);

            Console.WriteLine("training started");

            DateTime startTime = DateTime.UtcNow;

            for (int training_index = 0; training_index < training_number; training_index++)
                neural_network.run(training_index);

            double Training_time_s = (DateTime.UtcNow - startTime).TotalSeconds;

            Console.WriteLine("training ended");
            Console.WriteLine("training time is {0}s", Training_time_s);

            startTime = DateTime.UtcNow;

            double pass_rate = neural_network.test(test_image, test_label, testing_number);

            double Testing_time_s = (DateTime.UtcNow - startTime).TotalSeconds;            

            Console.WriteLine("pass rate is {0}%", pass_rate);
            Console.WriteLine("testing time is {0}s", Testing_time_s);

            Console.WriteLine("\nPress any key to continue...");
            Console.ReadKey();
        }



        /// <summary>
        /// used to decompress gz file
        /// </summary>
        /// <param name="fileToDecompress"></param>
        public static void Decompress(FileInfo fileToDecompress)
        {
            using (FileStream originalFileStream = fileToDecompress.OpenRead())
            {
                string currentFileName = fileToDecompress.FullName;
                string newFileName = currentFileName.Remove(currentFileName.Length - fileToDecompress.Extension.Length);

                using (FileStream decompressedFileStream = File.Create(newFileName))
                {
                    using (GZipStream decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress))
                    {
                        decompressionStream.CopyTo(decompressedFileStream);
                        Console.WriteLine("Decompressed: {0}", fileToDecompress.Name);
                    }
                }
            }
        }
    }
}
