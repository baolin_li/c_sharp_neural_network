﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetworkApplication
{
    public class Model
    {
        private int input_size;
        private int num_classes;
        private int batchSz;
        private double learning_rate;
        private byte[] train_images;
        private byte[] train_labels;
        public double[,] W;
        public double[] b;

        public Model (byte[] input_train_images, byte[] input_train_labels)
        {
            input_size = 784;
            num_classes = 10;
            batchSz = 1;
            learning_rate = 0.5;

            train_images = input_train_images;
            train_labels = input_train_labels;

            W = new double[input_size, num_classes]; // 784 * 10 array
            b = new double[num_classes]; // 1 * 10 array

            // randomize
            Random rnd = new Random();

            for (int i = 0; i < W.GetLength(0); i++)
            {
                for (int j = 0; j < W.GetLength(1); j++)
                    W[i, j] = rnd.NextDouble();
            }
            for (int i = 0; i < b.Length; i++)
                b[i] = rnd.NextDouble();
        }

        public void run (int image_index)
        {
            /*-------------- first do forward pass ------------------*/

            // create 1 * 784 array
            byte[] input_set_byte = train_images.Skip(image_index * input_size).Take(input_size).ToArray();
            double[] input_set = input_set_byte.Select(Convert.ToDouble).ToArray();
            input_set = input_set.Select(value => value / 255).ToArray();            
            double[,] input_set_2d = new double[1, input_size];
            for (int i = 0; i < input_size; i++)
                input_set_2d[0, i] = input_set[i];

            int label_set = train_labels[image_index];

            double[,] product = MultiplyMatrix(input_set_2d, W);

            // 1 * 10 array, use a list
            List<double> logits = new List<double>();
            for (int i = 0; i < num_classes; i++)
                logits.Add(b[i] + product[0, i]);

            var logits_exp = logits.Select(value => Math.Exp(value)).ToList();
            var prob_demon = logits_exp.Sum();
            var prob_list = logits_exp.Select(Value => Value / prob_demon).ToList();

            /*-------------- now do backward pass ------------------*/
            
            List<double> delta_b = new List<double>();
            for (int index = 0; index < prob_list.Count(); index++)
            {
                if (label_set == index)
                {
                    double delta = learning_rate * (1 - prob_list[index]);
                    delta_b.Add(delta);
                }
                else
                {
                    double delta = -1 * learning_rate * prob_list[index];
                    delta_b.Add(delta);
                }
            }

            for (int i = 0; i < num_classes; i++)
                b[i] += delta_b[i];

            double[,] delta_W = new double[input_size, num_classes]; // 784 * 10 array
            for (int column = 0; column < num_classes; column++)
            {
                for (int row = 0; row < input_size; row++)
                {
                    if (label_set == column)
                    {
                        double delta = learning_rate * (1 - prob_list[column]) * input_set[row];
                        delta_W[row, column] = delta;
                    }
                    else
                    {
                        double delta = -1 * learning_rate * prob_list[column] * input_set[row];
                        delta_W[row, column] = delta;
                    }
                }
            }

            for (int i = 0; i < W.GetLength(0); i++)
            {
                for (int j = 0; j < W.GetLength(1); j++)
                    W[i, j] += delta_W[i, j];
            }

        }

        public double test(byte[] test_images, byte[] test_labels, int testing_number)
        {
            int pass_num = 0;

            for (int test_index = 0; test_index < testing_number; test_index++)
            {
                byte[] test_input_set_byte = test_images.Skip(test_index * input_size).Take(input_size).ToArray();
                double[] test_input_set = test_input_set_byte.Select(Convert.ToDouble).ToArray();
                test_input_set = test_input_set.Select(value => value / 255).ToArray();
                double[,] test_input_set_2d = new double[1, input_size];

                for (int i = 0; i < input_size; i++)
                    test_input_set_2d[0, i] = test_input_set[i];

                int test_label_set = test_labels[test_index];

                double[,] test_product = MultiplyMatrix(test_input_set_2d, W);

                // 1 * 10 array, use a list
                List<double> test_logits = new List<double>();
                for (int i = 0; i < num_classes; i++)
                    test_logits.Add(b[i] + test_product[0, i]);

                var test_logits_exp = test_logits.Select(value => Math.Exp(value)).ToList();
                var test_prob_demon = test_logits_exp.Sum();
                var test_prob_list = test_logits_exp.Select(Value => Value / test_prob_demon).ToList();

                int NN_decision = test_prob_list.IndexOf(test_prob_list.Max());
                if (NN_decision == test_label_set)
                    pass_num += 1;
            }

            return (pass_num / (double)testing_number * 100);
        }

        /// <summary>
        /// https://stackoverflow.com/questions/6311309/how-can-i-multiply-two-matrices-in-c
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        double[,] MultiplyMatrix(double[,] A, double[,] B)
        {
            int rA = A.GetLength(0);
            int cA = A.GetLength(1);
            int rB = B.GetLength(0);
            int cB = B.GetLength(1);
            double temp = 0;
            double[,] kHasil = new double[rA, cB];
            if (cA != rB)
            {
                throw new Exception("matrik can't be multiplied !!");                                                    
            }
            else
            {
                for (int i = 0; i < rA; i++)
                {
                    for (int j = 0; j < cB; j++)
                    {
                        temp = 0;
                        for (int k = 0; k < cA; k++)
                        {
                            temp += A[i, k] * B[k, j];
                        }
                        kHasil[i, j] = temp;
                    }
                }
                return kHasil;
            }
        }
    }
}
